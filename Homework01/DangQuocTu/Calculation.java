package hello;

import java.util.Scanner;
public class Calculation {
	
  public static void getResult(int a,int b) {
	  System.out.println("Add 2 numbers:" + (a+b));
	  System.out.println("\nSubtract 2 numbers:" + (a-b));
	  System.out.println("\nMultiply 2 numbers:" + (a*b));
	  System.out.println("\nDivide 2 numbers:" + (a/b));
  }
  public static void main(String[] args) {
      
	  int a,b;
	  Scanner sc= new Scanner(System.in);
	  
	  System.out.print("Enter first number: ");  
	  a= sc.nextInt();
	  
	  do {
		  System.out.print("Enter second number: ");  
		  b= sc.nextInt();
		  if(b==0) {
			  System.out.println("Cannot divide by 0.");
		  }
	  }while(b==0);
	  
	  getResult(a,b);
		
  }
}
