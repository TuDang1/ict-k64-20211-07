package hello;
import java.util.Scanner;
public class HelloWorld {

	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in); //System.in is a standard input stream  
		System.out.print("Enter your name: ");  
		String str= sc.nextLine();              //reads string  
		System.out.print("Hello "+str+"!");    
       
	}

}
